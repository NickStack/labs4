﻿using Common.Infrastructrure;
using Foundatation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SP_l1
{
    internal class Program
    {
        private static FileInfo _lastFileInfo;
        private static IPAddress _lastConnectedClientIp;
        private static IPEndPoint _lastConnectedClientIpEndPoint;
        private static int _lastConnectedPort;
        private static HashSet<int> _lastConfirmedPackages = new HashSet<int>();
        private static readonly object ConfirmedPackagesLocker = new object();
        private static readonly HashSet<int> ConfirmPackages = new HashSet<int>();
        private static readonly HashSet<int> ConfirmedPackages = new HashSet<int>();
        private const int MinSize = 4096 * 15;

        private static async Task<int> Main()
        {
            while (true)
            {
                await RunServer();
            }
        }

        public static async Task RunServer()
        {
            Console.WriteLine("TCP(0), UDP(1), Multiplex TCP Server(2), Parallel TCP(3) Server?");
            var protocolNumber = Console.ReadKey();
            ProtocolType protocolType;
            var isParallelServer = false;
            var isMultiplexingServer = false;
            switch (protocolNumber.Key)
            //switch (ConsoleKey.D1)
            {
                case ConsoleKey.D0:
                    {
                        protocolType = ProtocolType.Tcp;
                        break;
                    }

                case ConsoleKey.D1:
                    {
                        protocolType = ProtocolType.Udp;
                        break;
                    }

                case ConsoleKey.D2:
                    {
                        protocolType = ProtocolType.Tcp;
                        isMultiplexingServer = true;
                        break;
                    }

                case ConsoleKey.D3:
                    {
                        protocolType = ProtocolType.Tcp;
                        isParallelServer = true;
                        break;
                    }

                default:
                    throw new ArgumentOutOfRangeException("Unknown type of server");
            }
            Console.WriteLine("Ïnserted ?");
            Console.ReadLine();
            var ipHost = Dns.GetHostEntry(Dns.GetHostName());
            var ipAddress = ipHost.AddressList[1];
            Console.WriteLine("Server ip address is {0}", ipHost.AddressList[1]);
            var serverIpEndPoint = new IPEndPoint(ipAddress, Constants.ServerPortNumber);
            switch (protocolType)
            {
                case ProtocolType.Tcp:
                    {
                        if (isMultiplexingServer)
                        {
                            RunTcpMultiplexingServer();

                            break;
                        }
                        if (isParallelServer)
                        {
                            RunTcpParallelServer();

                            break;
                        }

                        RunTcpServer(serverIpEndPoint);
                    }
                    break;
                case ProtocolType.Udp:
                    {
                        await RunUdpServer();

                        break;
                    }
            }
        }

        public static async void RunTcpServer(IPEndPoint ipEndPoint)
        {
            SocketConfigurator.ConfigureSocket(out var listener, ipEndPoint.Address.AddressFamily);
            try
            {
                listener.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                listener.Bind(ipEndPoint);
                listener.Listen(10);
                while (true)
                {
                    Console.WriteLine("Waiting connection ... ");
                    var clientSocket = listener.Accept();
                    while (true)
                    {
                        var bytes = new byte[1024];
                        string data = null;
                        while (true)
                        {
                            var numByte = clientSocket.Receive(bytes);
                            data += Encoding.ASCII.GetString(bytes, 0, numByte);
                            if (clientSocket.Available <= 0)
                            {
                                break;
                            }
                        }

                        Console.WriteLine("Text received -> {0} ", data);
                        var receivedData = data.Split(" ");
                        var method = EnumParser.ParseEnum<Methods>(receivedData[0]);
                        var content = receivedData.Length > 1 ? receivedData[1] : String.Empty;
                        await Execute(method, clientSocket, content);
                    }
                }
            }
            catch (IOException ioe)
            {
                Console.WriteLine(ioe.ToString());
                listener.Shutdown(SocketShutdown.Both);
                listener.Close();
            }
            catch (Exception)
            {
                listener.Close();
            }
        }

        public static void RunTcpMultiplexingServer()
        {
            var sockList = new ArrayList(Constants.MaxSocketsCount);
            var main = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            var serverIpEndPoint = new IPEndPoint(IPAddress.Any, Constants.ServerPortNumber);

            main.Bind(serverIpEndPoint);
            main.Listen(Constants.MaxSocketsCount);
            var tm = new TimerCallback(ExecuteRequest);
            var timer = new Timer(tm, sockList, 0, 2000);
            while (true)
            {
                for (var i = 0; i < Constants.MaxSocketsCount; i++)
                {
                    Console.WriteLine("Waiting for " + (Constants.MaxSocketsCount - i) + " clients...");
                    var client = main.Accept();

                    var ipEndPoint = (IPEndPoint)client.RemoteEndPoint;
                    Console.WriteLine("Connected to {0}", ipEndPoint);
                    sockList.Add(client);
                }
            }
        }

        public static async void ExecuteRequest(object sockets)
        {
            var socketsList = (ArrayList)sockets;
            var copyList = new ArrayList(socketsList);
            Console.WriteLine("Monitoring {0} sockets...", copyList.Count);
            if (copyList.Count == 0)
            {
                return;
            }
            Socket.Select(copyList, null, null, 10000000);
            foreach (Socket clientSocket in copyList)
            {
                var bytes = new byte[1024];
                string data = null;
                while (true)
                {
                    var numByte = clientSocket.Receive(bytes);
                    data += Encoding.ASCII.GetString(bytes, 0, numByte);
                    if (clientSocket.Available <= 0)
                    {
                        break;
                    }
                }

                Console.WriteLine("Text received -> {0} ", data);
                var receivedData = data.Split(" ");
                var method = EnumParser.ParseEnum<Methods>(receivedData[0]);
                var content = receivedData.Length > 1 ? receivedData[1] : String.Empty;
                await Execute(method, clientSocket, content);
                clientSocket.Close();
            }
            socketsList.Clear();
        }

        public static void RunTcpParallelServer()
        {
            var main = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            var serverIpEndPoint = new IPEndPoint(IPAddress.Any, Constants.ServerPortNumber);

            main.Bind(serverIpEndPoint);
            main.Listen(Constants.MaxSocketsCount);
            ThreadPool.SetMinThreads(1, 1);
            ThreadPool.SetMaxThreads(Constants.MaxSocketsCount, Constants.MaxSocketsCount);

            while (true)
            {
                var client = main.Accept();

                var ipEndPoint = (IPEndPoint)client.RemoteEndPoint;
                Console.WriteLine("Connected to {0}", ipEndPoint);
                ThreadPool.QueueUserWorkItem(ExecuteParallelRequest, client);
            }
        }

        public static async void ExecuteParallelRequest(object socket)
        {
            var client = (Socket)socket;
            var bytes = new byte[1024];
            string data = null;
            while (true)
            {
                var numByte = client.Receive(bytes);
                data += Encoding.ASCII.GetString(bytes, 0, numByte);
                if (client.Available <= 0)
                {
                    break;
                }
            }

            Console.WriteLine("Text received -> {0} ", data);
            var receivedData = data.Split(" ");
            var method = EnumParser.ParseEnum<Methods>(receivedData[0]);
            var content = receivedData.Length > 1 ? receivedData[1] : String.Empty;
            await Execute(method, client, content);
            client.Close();
        }

        public static async Task RunUdpServer()
        {
            try
            {
                await ReceiveMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        private static async Task ReceiveMessage()
        {
            var receiver = new UdpClient(Constants.ServerPortNumber) { Client = { ReceiveBufferSize = MinSize + 4096 } };
            IPEndPoint clientIpEndPoint = null;
            try
            {
                while (true)
                {
                    Console.WriteLine("Waiting connection ... ");
                    var data = receiver.Receive(ref clientIpEndPoint);
                    var message = Encoding.Unicode.GetString(data);
                    Console.WriteLine("Message From Client: {0}", message);
                    var receivedData = message.Split(" ");
                    var method = EnumParser.ParseEnum<Methods>(receivedData[0]);
                    var content = receivedData.Length > 1 ? receivedData[1] : String.Empty;

                    await ExecuteUdp(receiver, clientIpEndPoint, method, content);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                receiver.Close();
            }
        }

        private static async Task Execute(Methods method, Socket clientSocket, string content = null)
        {
            var serverApi = new ServerApi();

            switch (method)
            {
                case Methods.Close:
                    {
                        clientSocket.Send(Encoding.ASCII.GetBytes(Constants.ConnectionWasClosed));
                        clientSocket.Shutdown(SocketShutdown.Both);
                        clientSocket.Close();

                        return;
                    }

                case Methods.Download:
                    {
                        var contentItems = content.Split('|');
                        var fileName = contentItems[0];
                        var seek = Convert.ToInt64(contentItems[1]);
                        var filePath = Path.Combine(Constants.ServerFolderPath, fileName);
                        if (!File.Exists(filePath))
                        {
                            clientSocket.Send(Encoding.ASCII.GetBytes(Constants.FileDoesntExist + Constants.Eof));

                            return;
                        }

                        var fileInfo = new FileInfo(filePath);
                        var isSuccessful = await SendFileAsync(fileInfo, clientSocket, seek);
                        if (isSuccessful)
                        {
                            return;
                        }
                        clientSocket.Shutdown(SocketShutdown.Both);
                        clientSocket.Close();

                        return;
                    }

                default:
                    {
                        var answer = serverApi.ExecuteCommand(method, content);
                        {
                            var message = Encoding.ASCII.GetBytes(answer);
                            clientSocket.Send(message);
                        }

                        return;
                    }
            }
        }


        private static async Task ExecuteUdp(UdpClient client, IPEndPoint clientIpEndPoint, Methods method, string content)
        {
            switch (method)
            {
                case Methods.Close:
                    {
                        await SendMessageAsync(client, clientIpEndPoint, Constants.ConnectionWasClosed);
                        client.Close();

                        return;
                    }

                case Methods.Echo:
                    {
                        await SendMessageAsync(client, clientIpEndPoint, content);

                        return;
                    }

                case Methods.Time:
                    {
                        await SendMessageAsync(client, clientIpEndPoint, DateTime.Now.ToString(CultureInfo.InvariantCulture));

                        return;
                    }

                case Methods.Download:
                    {
                        Task.Run(async () =>
                      {
                          var contentItems = content.Split('|');
                          var fileName = contentItems[0];
                          var seek = Convert.ToInt64(contentItems[1]);
                          var filePath = Path.Combine(Constants.ServerFolderPath, fileName);
                          if (!File.Exists(filePath))
                          {
                              await SendMessageAsync(client, clientIpEndPoint, Constants.FileDoesntExist + Constants.Eof);

                              return;
                          }

                          var fileInfo = new FileInfo(filePath);
                          var isSuccessful = await SendFileUdpAsync(fileInfo, client, clientIpEndPoint);
                          if (isSuccessful)
                          {
                              return;
                          }
                          client.Close();
                      });

                        return;
                    }

                case Methods.PackageReceived:
                    {
                        var contentItems = content.Split('|');
                        lock (ConfirmedPackagesLocker)
                        {
                            ConfirmedPackages.Add(Convert.ToInt32(contentItems[0]));
                        }

                        return;
                    }
            }
        }


        private static async Task<bool> SendFileUdpAsync(FileInfo file, UdpClient sender, IPEndPoint clientEndPoint)
        {
            var isSuccessful = true;
            var readed = MinSize;
            var buffer = new byte[MinSize];
            try
            {
                long eofPackageNumber;
                using (var fileStream = file.OpenRead())
                {
                    lock (ConfirmedPackagesLocker)
                    {
                        var packagesCount = fileStream.Length / MinSize;
                        if (fileStream.Length % MinSize != 0)
                        {
                            packagesCount += 1;
                        }

                        eofPackageNumber = packagesCount + 1;
                        for (var i = 0; i < packagesCount; i++)
                        {
                            ConfirmPackages.Add(i);
                        }
                    }
                }

                var shouldSendPartOfFile = false;

                using (var fileStream = file.OpenRead())
                {
                    var stopWatch = new Stopwatch();
                    stopWatch.Start();
                    do
                    {
                        if (_lastConnectedClientIpEndPoint?.ToString() == clientEndPoint.ToString()
                            && _lastFileInfo.FullName == file.FullName)
                        {
                            ConfirmPackages.RemoveWhere(item => _lastConfirmedPackages.Contains(item));
                        }

                        foreach (var packageNumber in ConfirmPackages)
                        {
                            fileStream.Seek(MinSize * packageNumber, SeekOrigin.Begin);
                            while (readed != 0)
                            {
                                readed = fileStream.Read(buffer, 0, buffer.Length);
                                var packageNumberAsByte = BitConverter.GetBytes(packageNumber);
                                var package = Combine(packageNumberAsByte, buffer);
                                await SendMessageAsync(sender, clientEndPoint, package);
                                if (shouldSendPartOfFile)
                                {
                                    break;
                                }
                            }

                            readed = 1;
                            shouldSendPartOfFile = true;
                        }

                        await Task.Delay(200);
                        lock (ConfirmedPackagesLocker)
                        {
                            ConfirmPackages.RemoveWhere(i => ConfirmedPackages.Contains(i));
                            Console.WriteLine("Not confirmed yet = " + ConfirmPackages.Count);
                        }
                    } while (ConfirmPackages.Count > 0);

                    _lastConnectedClientIpEndPoint = clientEndPoint;
                    _lastFileInfo = file;
                    _lastConfirmedPackages.Clear();

                    stopWatch.Stop();
                    Console.WriteLine("Speed = " + fileStream.Length / stopWatch.ElapsedMilliseconds + "byte/ms");
                }

                var number = BitConverter.GetBytes(eofPackageNumber);
                var m = Encoding.Unicode.GetBytes(Constants.Eof);
                var eofPackage = Combine(number, m);
                await SendMessageAsync(sender, clientEndPoint, eofPackage);
            }
            catch (IOException e)
            {
                _lastConnectedClientIpEndPoint = clientEndPoint;
                _lastFileInfo = file;
                _lastConfirmedPackages = ConfirmedPackages;
                isSuccessful = false;
            }

            return isSuccessful;
        }

        private static Task SendMessageAsync(UdpClient sender, IPEndPoint ipEndPoint, string content)
        {
            try
            {
                var data = Encoding.Unicode.GetBytes(content);
                sender.Send(data, data.Length, ipEndPoint.Address.ToString(), Constants.UdpClientPortNumber);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return Task.CompletedTask;
        }

        private static Task SendMessageAsync(UdpClient sender, IPEndPoint ipEndPoint, byte[] content)
        {
            try
            {
                sender.Send(content, content.Length, ipEndPoint.Address.ToString(), Constants.UdpClientPortNumber);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return Task.CompletedTask;
        }

        public static async Task<bool> SendFileAsync(FileInfo file, Socket socket, long seek)
        {
            var isSuccessful = true;
            const int minSize = 4096;
            var readed = minSize;
            var buffer = new byte[minSize];
            var remoteIpAddress = (socket.RemoteEndPoint as IPEndPoint)?.Address;
            var a = new NetworkStream(socket, FileAccess.Write, true);
            using (var networkStream = new BufferedStream(a))
            {
                try
                {
                    using (var fileStream = file.OpenRead())
                    {
                        var stopWatch = new Stopwatch();
                        stopWatch.Start();
                        _lastConnectedClientIp = remoteIpAddress;
                        _lastFileInfo = file;
                        if (_lastConnectedClientIp?.ToString() == remoteIpAddress?.ToString() && _lastFileInfo.FullName == file.FullName)
                        {
                            fileStream.Seek(seek, SeekOrigin.Begin);
                        }

                        var packageNumber = 0;
                        while (readed != 0)
                        {
                            //if (packageNumber % 200 == 0)
                            //{
                            //    var urgencyFlag = Encoding.ASCII.GetBytes(Constants.Urg + "|" + "Urgency command execute, please" + "|");
                            //    var ret = new byte[urgencyFlag.Length];

                            //    Array.Copy(urgencyFlag, 0, ret, 0, urgencyFlag.Length);

                            //    buffer = ret;
                            //    readed = ret.Length;
                            //    packageNumber++;
                            //    networkStream.Write(buffer, 0, readed);
                            //    continue;
                            //}
                            readed = fileStream.Read(buffer, 0, buffer.Length);
                            packageNumber++;
                            networkStream.Write(buffer, 0, readed);
                        }

                        _lastConnectedClientIp = null;
                        var stringEnd = Encoding.ASCII.GetBytes(Constants.Eof);
                        networkStream.Write(stringEnd, 0, stringEnd.Length);
                        await networkStream.FlushAsync();
                        stopWatch.Stop();
                        Console.WriteLine("Speed = " + fileStream.Length / stopWatch.ElapsedMilliseconds + "byte/ms");
                    }
                }
                catch (IOException)
                {
                    _lastFileInfo = file;
                    _lastConnectedClientIp = remoteIpAddress;
                    isSuccessful = false;
                }
            }

            return isSuccessful;
        }
        public static byte[] Combine(byte[] first, byte[] second)
        {
            var ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);

            return ret;
        }
    }
}