﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using Common.Infrastructrure;
using Foundatation.Interfaces;

namespace Foundatation
{
    public class ServerApi : IServerApi
    {
        public string ExecuteCommand(Methods method, string content)
        {
            switch (method)
            {
                case Methods.Echo:
                    return ExecuteEchoCommandAsync(content);
                case Methods.Time:
                    return ExecuteTimeCommand();
                case Methods.Download:
                    return ExecuteDownloadCommand(content);
                default:
                    throw new ArgumentOutOfRangeException(nameof(method), method, "method out of range");
            }
        }


        private string ExecuteEchoCommandAsync(string content)
        {
            return content;
        }

        private string ExecuteTimeCommand()
        {
            return DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }

        private string ExecuteDownloadCommand(string fileName)
        {
            var isFileExist = FileExistsRecursive(Constants.ServerFolderPath, fileName);
            if (!isFileExist)
            {
                return "File doesn't exist";
            }
            var fullPath = Path.Combine(Constants.ServerFolderPath, fileName);
            //var file = File.ReadAllBytes(fullPath);
            //var base64String = Convert.ToBase64String(file);
            //var extension = Path.GetExtension(fileName)?.Remove(0, 1);
            //var contentType = EnumParser.ParseEnum<ContentTypes>(extension);
            //var answer = new Answer(base64String, contentType);
            //var serializedAnswer = JsonConvert.SerializeObject(answer);

            return fullPath;
        }

        private bool FileExistsRecursive(string rootPath, string filename)
        {
            if (File.Exists(Path.Combine(rootPath, filename)))
            {
                return true;
            }

            return Directory.GetDirectories(rootPath).Select(subDir => FileExistsRecursive(subDir, filename)).FirstOrDefault();
        }
    }
}