﻿using Common.Infrastructrure;

namespace Foundatation.Interfaces
{
    public interface IServerApi
    {
        string ExecuteCommand(Methods method, string content);
    }
}