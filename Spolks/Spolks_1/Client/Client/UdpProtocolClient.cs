﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Common.Infrastructrure;

namespace Client
{
    public class UdpProtocolClient : IClient
    {
        private static readonly UdpClient Sender;
        private static bool _continueDownload;
        private static string _downloadCommandContent;
        private static readonly object ReceivedPackagesLocker = new object();
        private static readonly HashSet<int> ReceivedPackages = new HashSet<int>();
        private static readonly HashSet<int> ConfirmedPackages = new HashSet<int>();
        private const int MinSize = 4096 * 15;

        static UdpProtocolClient()
        {
            Sender = new UdpClient(Constants.UdpClientPortNumber) { Client = { ReceiveBufferSize = (MinSize + 4096) } };
        }


        public async Task<bool> RunClientAsync(IPEndPoint serverIpEndPoint, IPEndPoint clientIpEndPoint, Methods method)
        {
            SendMessage(serverIpEndPoint, method);

            return true;
        }


        public static void SendMessage(IPEndPoint serverIpEndPoint, Methods method)
        {
            try
            {
                var message = CreateCommandFrom(method);
                var data = Encoding.Unicode.GetBytes(message);
                Sender.Send(data, data.Length, serverIpEndPoint.Address.ToString(), Constants.ServerPortNumber);
                if (method == Methods.Download)
                {
                    var content = message.Replace(Constants.Method.Download + " ", "");
                    var path = Path.Combine(Constants.ClientFolderPath, content.Split("|")[0]);
                    var fileInfo = new FileInfo(path);
                    ReceiveFile(Sender, fileInfo);
                }
                else
                {
                    ReceiveMessage(Sender, serverIpEndPoint);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void ReceiveMessage(UdpClient receiver, IPEndPoint serverIpEndPoint)
        {
            try
            {
                Console.WriteLine("Waiting answer ... ");
                var data = receiver.Receive(ref serverIpEndPoint);
                var message = Encoding.Unicode.GetString(data);
                Console.WriteLine("Message From Server: {0}", message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void ReceiveFile(UdpClient receiver, FileInfo file)
        {
            IPEndPoint serverIpEndPoint = null;
            try
            {
                using (var fileStream = file.OpenWrite())
                {
                    while (true)
                    {
                        Console.WriteLine("Waiting answer ... ");
                        var data = receiver.Receive(ref serverIpEndPoint);
                        var packageNumber = BitConverter.ToInt32(data.Take(4).ToArray());

                        var privilegeData = data.Skip(data.Length - 4).ToArray();
                        var privilegeDataContent = Encoding.Unicode.GetString(privilegeData);

                        lock (ReceivedPackagesLocker)
                        {
                            if (ReceivedPackages.Contains(packageNumber))
                            {
                                SendConfirmation(serverIpEndPoint, packageNumber);
                                continue;
                            }

                            ReceivedPackages.Add(packageNumber);
                        }

                        var contentLength = data.Length - 4;
                        var filteredBuffer = new byte[contentLength];
                        Array.Copy(data, 4, filteredBuffer, 0, contentLength);
                        var content = Encoding.Unicode.GetString(filteredBuffer);
                        if (content.Contains(Constants.Eof))
                        {
                            break;
                        }

                        fileStream.Seek(MinSize * packageNumber, SeekOrigin.Begin);
                        fileStream.Write(filteredBuffer, 0, contentLength);

                        SendConfirmation(serverIpEndPoint, packageNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public static void SendConfirmation(IPEndPoint serverIpEndPoint, int packageNumber)
        {
            var data = Encoding.Unicode.GetBytes(Methods.PackageReceived + " " + packageNumber);
            Sender.Send(data, data.Length, serverIpEndPoint.Address.ToString(), Constants.ServerPortNumber);
            ConfirmedPackages.Add(packageNumber);
        }


        private static string CreateCommandFrom(Methods method)
        {
            switch (method)
            {
                case Methods.Time:
                    {
                        return Constants.Method.Time;
                    }

                case Methods.Close:
                    {
                        return Constants.Method.Close;
                    }

                case Methods.Echo:
                    {
                        string message;
                        do
                        {
                            Console.WriteLine("Enter message");
                            message = Console.ReadLine();
                        } while (string.IsNullOrEmpty(message));

                        return Constants.Method.Echo + " " + message;
                    }

                case Methods.Download:
                    {
                        string fileName = null;
                        long position = 0;
                        try
                        {
                            if (!_continueDownload)
                            {
                                bool shouldContinue;
                                do
                                {
                                    Console.WriteLine("Enter file name");
                                    fileName = Console.ReadLine();
                                    shouldContinue = string.IsNullOrEmpty(fileName) || !fileName.Contains(".");
                                } while (shouldContinue);
                            }
                            else
                            {
                                var path = Path.Combine(Constants.ClientFolderPath, _downloadCommandContent.Split("|")[0]);
                                var fileInfo = new FileInfo(path);
                                using (var stream = fileInfo.AppendText().BaseStream)
                                {
                                    position = stream.Position;
                                    stream.Close();
                                }

                                fileName = _downloadCommandContent.Split("|")[0];
                            }

                            _continueDownload = false;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                        _downloadCommandContent = fileName + "|" + position;

                        return Constants.Method.Download + " " + _downloadCommandContent;
                    }

                default:
                    throw new ArgumentOutOfRangeException(nameof(method), method, "Method is out of range");
            }
        }
    }
}