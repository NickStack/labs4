﻿using Common.Infrastructrure;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class TcpClient : IClient
    {
        private Socket _sender;
        private bool _continueDownload;
        private bool _wasClosed;
        private string _downloadCommandContent;


        public async Task<bool> RunClientAsync(IPEndPoint serverIpEndPoint, IPEndPoint clientIpEndPoint, Methods method)
        {
            do
            {
                try
                {
                    if (_sender == null || _continueDownload || _wasClosed)
                    {
                        _wasClosed = false;
                        ConnectSocket(serverIpEndPoint, clientIpEndPoint);
                    }
                    Console.WriteLine("Socket connected to -> {0} ", _sender.RemoteEndPoint);
                    var command = CreateCommandFrom(method);
                    var messageSent = Encoding.ASCII.GetBytes(command);
                    _sender.Send(messageSent);
                    ExecuteMethod(method);
                    if (method == Methods.Close)
                    {
                        return false;
                    }
                    _continueDownload = false;
                }
                catch (IOException)
                {
                    Console.WriteLine("Connection failed");
                    Console.WriteLine("Do you want to continue?(y/n)");
                    var answer = Console.ReadKey();
                    _continueDownload = answer.Key == ConsoleKey.Y;
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se);
                    _wasClosed = true;

                    return false;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e);
                    _wasClosed = true;

                    return false;
                }
            } while (_continueDownload);

            return true;
        }

        public async Task<bool> RunTestClientAsync(IPEndPoint serverIpEndPoint, IPEndPoint clientIpEndPoint, string content, Methods method)
        {
            do
            {
                try
                {
                    if (_sender == null || _continueDownload || _wasClosed)
                    {
                        _wasClosed = false;
                        ConnectSocket(serverIpEndPoint, clientIpEndPoint);
                    }
                    Console.WriteLine("Socket connected to -> {0} ", _sender.RemoteEndPoint);
                    var messageSent = Encoding.ASCII.GetBytes(method + " " + content);
                    _sender.Send(messageSent);
                    ExecuteMethod(method, content);
                    if (method == Methods.Close)
                    {
                        return false;
                    }
                    _continueDownload = false;
                }
                catch (IOException)
                {
                    Console.WriteLine("Connection failed");
                    Console.WriteLine("Do you want to continue?(y/n)");
                    var answer = Console.ReadKey();
                    _continueDownload = answer.Key == ConsoleKey.Y;
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se);
                    _wasClosed = true;

                    return false;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e);
                    _wasClosed = true;

                    return false;
                }
            } while (_continueDownload);

            return true;
        }


        private void ExecuteMethod(Methods method, string content = null)
        {
            var bytes = new byte[1024];
            string data = null;

            switch (method)
            {
                case Methods.Download:
                    {
                        _downloadCommandContent = content ?? _downloadCommandContent;
                        var path = Path.Combine(Constants.ClientFolderPath, _downloadCommandContent.Split("|")[0]);
                        var fileInfo = new FileInfo(path);
                        var isSuccessful = ReceiveFile(_sender, fileInfo);
                        if (isSuccessful)
                        {
                            Console.WriteLine("file was downloaded");
                        }

                        return;
                    }

                case Methods.Close:
                    {
                        _sender.Shutdown(SocketShutdown.Both);
                        _sender.Close();
                        _wasClosed = true;

                        Console.WriteLine("closed");

                        return;
                    }

                default:
                    {
                        while (true)
                        {
                            var numByte = _sender.Receive(bytes);
                            data += Encoding.ASCII.GetString(bytes, 0, numByte);
                            if (_sender.Available <= 0)
                            {
                                break;
                            }
                        }
                        Console.WriteLine("Content from Server -> {0}", Encoding.ASCII.GetString(bytes, 0, data.Length));

                        return;
                    }
            }
        }

        private bool ReceiveFile(Socket socket, FileInfo file)
        {
            const int minSize = 4096 * 1024;
            var readed = minSize;
            var buffer = new byte[minSize];
            var shouldRemove = false;
            using (var fileStream = file.AppendText())
            {
                using (var networkStream = new NetworkStream(socket, false))
                {
                    var stopWatch = new Stopwatch();
                    stopWatch.Start();
                    while (readed != 0)
                    {
                        readed = networkStream.Read(buffer, 0, buffer.Length);
                        var readString = Encoding.ASCII.GetString(buffer, 0, readed);

                        if (readString.Contains(Constants.FileDoesntExist))
                        {
                            shouldRemove = true;
                        }

                        if (readString.Contains(Constants.Urg))
                        {
                            var parts = readString.Split("|");

                            readString = readString.Replace(Constants.Urg, "");
                            readString = readString.Replace("|", "");
                            Console.WriteLine(parts[1]);
                        }

                        if (readString.Contains(Constants.Eof))
                        {
                            byte[] filteredBuffer = new byte[buffer.Length - 5];
                            Array.Copy(buffer, filteredBuffer, buffer.Length - 5);
                            fileStream.BaseStream.Write(filteredBuffer, 0, readed);

                            break;
                        }

                        fileStream.BaseStream.Write(buffer, 0, readed);
                    }
                    stopWatch.Stop();
                    if (stopWatch.ElapsedMilliseconds != 0)
                    {
                        Console.WriteLine("Speed = " + fileStream.BaseStream.Length / stopWatch.ElapsedMilliseconds + "byte/ms");
                    }
                }
            }
            if (shouldRemove)
            {
                file.Delete();
            }

            return shouldRemove;
        }

        private void ConnectSocket(IPEndPoint serverIpEndPoint, IPEndPoint clientIpEndPoint)
        {
            _sender = new Socket(clientIpEndPoint.Address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            SocketConfigurator.ConfigureSocket(out _sender, clientIpEndPoint.Address.AddressFamily);
            _sender.Connect(serverIpEndPoint);
        }

        private string CreateCommandFrom(Methods method, string content = null)
        {
            switch (method)
            {
                case Methods.Time:
                    {
                        return Constants.Method.Time;
                    }

                case Methods.Close:
                    {
                        return Constants.Method.Close;
                    }

                case Methods.Echo:
                    {
                        string message;
                        do
                        {
                            Console.WriteLine("Enter message");
                            message = Console.ReadLine();
                        } while (string.IsNullOrEmpty(message));

                        return Constants.Method.Echo + " " + message;
                    }

                case Methods.Download:
                    {
                        var fileName = content;
                        long position = 0;
                        try
                        {
                            if (!_continueDownload)
                            {
                                bool shouldContinue;
                                do
                                {
                                    Console.WriteLine("Enter file name");
                                    fileName = Console.ReadLine();
                                    shouldContinue = string.IsNullOrEmpty(fileName) || !fileName.Contains(".");
                                } while (shouldContinue);
                            }
                            else
                            {
                                var path = Path.Combine(Constants.ClientFolderPath, _downloadCommandContent.Split("|")[0]);
                                var fileInfo = new FileInfo(path);
                                using (var stream = fileInfo.AppendText().BaseStream)
                                {
                                    position = stream.Position;
                                    stream.Close();
                                }

                                fileName = _downloadCommandContent.Split("|")[0];
                            }

                            _continueDownload = false;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                        _downloadCommandContent = fileName + "|" + position;

                        return Constants.Method.Download + " " + _downloadCommandContent;
                    }

                default:
                    throw new ArgumentOutOfRangeException(nameof(method), method, "Method is out of range");
            }
        }
    }
}