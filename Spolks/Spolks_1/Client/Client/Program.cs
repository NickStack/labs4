﻿using Common.Infrastructrure;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        private static IPAddress _ipAddress;
        private static IPEndPoint _serverIpEndPoint;
        private static IPEndPoint _clientIpEndPoint;

        public static async Task<int> Main()
        {
            var shouldInitialize = true;
            try
            {
                Console.WriteLine("TCP(0), UDP(1), Multiplexing Test(2), Parallel Test(3)?");
                var protocolNumber = Console.ReadKey();
                IClient client = null;
                ProtocolType protocolType;
                var isParallelTest = false;
                var isMultiplexingTest = false;
                switch (protocolNumber.Key)
                //switch (ConsoleKey.D1)
                {
                    case ConsoleKey.D0:
                        {
                            client = new TcpClient();
                            protocolType = ProtocolType.Tcp;

                            break;
                        }

                    case ConsoleKey.D1:
                        {
                            client = new UdpProtocolClient();
                            protocolType = ProtocolType.Udp;

                            break;
                        }

                    case ConsoleKey.D2:
                        {
                            protocolType = ProtocolType.Tcp;
                            isMultiplexingTest = true;

                            break;
                        }

                    case ConsoleKey.D3:
                        {
                            protocolType = ProtocolType.Tcp;
                            isParallelTest = true;

                            break;
                        }

                    default:
                        {
                            throw new ArgumentOutOfRangeException("No no");
                        }
                };

                while (true)
                {
                    if (shouldInitialize)
                    {
                        InitializeConnection(protocolType);
                    }

                    Console.WriteLine("Echo(0), Time(1), Close(2), Download(3)");

                    var method = (Methods)Convert.ToInt32(Console.ReadLine());
                    var serverIpEndPoint = new IPEndPoint(_ipAddress, Constants.ServerPortNumber);
                    if (protocolType == ProtocolType.Tcp && isMultiplexingTest)
                    {
                        //for (var i = 1; i <= 2; i++)
                        //{
                        //    var clientIpEndPoint = new IPEndPoint(_ipAddress, Constants.TcpClientPortNumber + i);
                        //    Task.Run(async () =>
                        //    {
                        //        var tcpClient = new TcpClient();
                        //        await tcpClient.RunClientAsync(serverIpEndPoint, clientIpEndPoint, method);
                        //    });
                        //}
                        var clientIpEndPoint1 = new IPEndPoint(_ipAddress, Constants.TcpClientPortNumber + 1);
                        var clientIpEndPoint2 = new IPEndPoint(_ipAddress, Constants.TcpClientPortNumber + 2);

                        Task.Run(async () =>
                        {
                            var tcpClient1 = new TcpClient();
                            tcpClient1.RunTestClientAsync(serverIpEndPoint, clientIpEndPoint1, "83mb.txt|0", Methods.Download);
                        });
                        Task.Run(async () =>
                        {
                            var tcpClient1 = new TcpClient();
                            tcpClient1.RunTestClientAsync(serverIpEndPoint, clientIpEndPoint2, "14mb.txt|0", Methods.Download);
                        });

                        shouldInitialize = false;
                    }
                    else if (protocolType == ProtocolType.Tcp && isParallelTest)
                    {
                        var clientIpEndPoint1 = new IPEndPoint(_ipAddress, Constants.TcpClientPortNumber + 1);
                        var clientIpEndPoint2 = new IPEndPoint(_ipAddress, Constants.TcpClientPortNumber + 2);

                        Task.Run(async () =>
                        {
                            var tcpClient1 = new TcpClient();
                            tcpClient1.RunTestClientAsync(serverIpEndPoint, clientIpEndPoint1, "2gb.txt|0", Methods.Download);
                        });
                        Task.Run(async () =>
                        {
                            var tcpClient1 = new TcpClient();
                            tcpClient1.RunTestClientAsync(serverIpEndPoint, clientIpEndPoint2, "14mb.txt|0", Methods.Download);
                        });
                    }
                    else
                    {
                        var isSuccessful = await client?.RunClientAsync(_serverIpEndPoint, _clientIpEndPoint, method);
                        shouldInitialize = !isSuccessful;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return 0;
        }

        public static void InitializeConnection(ProtocolType protocolType)
        {
            var hostName = Dns.GetHostName();
            //var hostName = "DESKTOP-AI0J7HS";
            var ipHost = Dns.GetHostEntry(hostName);
            Console.WriteLine("Input server ip address");
            var ip = Console.ReadLine(); //"169.254.113.66";//"192.168.8.51";/* Console.ReadLine();*///"169.254.113.66";
            var ipAddressString = ip.ToCharArray();
            var ipAddress = IPAddress.Parse(ipAddressString);
            _ipAddress = ipAddress;//ipHost.AddressList[1];//
            _serverIpEndPoint = new IPEndPoint(_ipAddress, Constants.ServerPortNumber);
            _clientIpEndPoint = new IPEndPoint(_ipAddress, protocolType == ProtocolType.Tcp ? Constants.TcpClientPortNumber : Constants.UdpClientPortNumber);
        }

    }
}