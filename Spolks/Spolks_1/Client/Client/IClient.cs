﻿using System.Net;
using System.Threading.Tasks;
using Common.Infrastructrure;

namespace Client
{
    public interface IClient
    {
        Task<bool> RunClientAsync(IPEndPoint serverIpEndPoint, IPEndPoint clientIpEndPoint, Methods method);
    }
}
