/****** Скрипт для команды SelectTopNRows из среды SSMS  ******/
-- Task 4.1
-- Информацию о сотрудниках у которых в номере паспорта 3 
-- конкретные цифры и вывести модифицировава имя и фамилию в один столбец 
-- (инициал. Пробел фамилия) и должность сотрудника и отсортировать по номеру паспорта.

SELECT TOP (1000) [Id]
      ,[PositionId]
      ,SUBSTRING([FirstName],1,1) + '.' + [SecondName] as firstAndLastName
      ,[PassportNumber]
      ,[DateOfBirthday]
  FROM [BankDB].[dbo].[Employee]
where PassportNumber like '%186%'
order by PassportNumber

-- Task 4.2
-- Вывести данные об окозании услег за определенный период т.е чтобы было видно кому ,\
-- кто и что за услуга(даты услуги и т.д) все отсортировать по дате оказания
--INSERT INTO [dbo].[Service] 
--(Comment,Date,TypeServiceId,EmployeeId,ClientId)
--VALUES 
--('Check Balans','20180518 10:37:09 AM', 4, 2, 3),
--('EUR to USD','20030512 08:38:09 AM', 6, 2, 2),
--('RUB to UAH','19990408 12:11:09 AM', 6, 3, 5),
--('Approve Credit','20050323 11:00:09 AM', 1, 8, 9),
--('Approve Credit','20060721 11:09:09 AM', 1, 5, 3)
--go

SELECT TOP (1000) Service.[Id] as ServiceId
      ,[Comment]
      ,[Date]
      ,Employee.FirstName + ' ' + Employee.SecondName as 'Employee Info'
      ,Client.Name as 'Client Name'
	  ,ServiceType.Name as 'Service name'
  FROM [BankDB].[dbo].[Service] as Service
  inner join [dbo].[Employee] as Employee on Employee.Id = EmployeeId
  inner join [BankDB].[dbo].[Client] as Client on Client.Id = ClientId
  inner join [dbo].[ServiceType] as ServiceType on ServiceType.Id = TypeServiceId
  where Date BETWEEN '2003-01-01' AND '2008-07-31'
  order by Date desc

-- Task 4.3
-- Найти комбинации между клиентом и филиалом не нужны дубликаты,
-- если нету счетов показать, отсортировать по фио и названию филиала
  
  SELECT TOP (1000)  
      [ClientId]
	  ,[FiolationId]
	  ,Account.[Id] as AccountId
      ,[AccountTypeId]
	  ,Client.Name
	  ,Fialation.Address
	  ,[balance]
  FROM [BankDB].[dbo].[Account] as Account
  join [BankDB].[dbo].[Client] as Client on Client.Id = AccountId
  join [BankDB].[dbo].[Fialation] as Fialation on Fialation.Id = Account.FiolationId
  group by [ClientId],[FiolationId], Account.[Id], [AccountTypeId],Client.Name,Fialation.Address, balance
-- Task 4.4
-- Вывести какие типы счетов не использованы, использовать только join.
  
  SELECT accountType.Name
  FROM [BankDB].[dbo].[Account] as account
  right join [BankDB].[dbo].[AccountType] as accountType on accountType.Id = account.[AccountTypeId]
  group by accountType.Name
  Having count(accountType.Id) = 1



  
--5 лаба

--Task 5.1.Нужно найти самый богатый филиал по текущему баланса на текущих счетах клиента
 
  SELECT TOP (1) 
      [FiolationId]
	  ,Sum([balance]) as balanceSum
	  ,Fiolation.RegionName
  FROM [BankDB].[dbo].[Account]
  inner join [BankDB].[dbo].[Fialation] as Fiolation on [FiolationId] = Fiolation.Id
  group by [FiolationId], Fiolation.RegionName

--Task 5.2.Найти для каждого города сколько клиентов и какой у них суммарный баланс и
--сделать обобщение сколько всего клиентов и средний баланс по городам

 SELECT [CityId]
	  , Count([Client].Id) as CityClientsCount
	  , Sum([Account].balance) as CityClientsBalans
	  , [City].Address 
  FROM [BankDB].[dbo].[Client]
  inner join [BankDB].[dbo].[City] as City on City.Id = CityId 
  inner join [BankDB].[dbo].[Account] as Account on Account.Id = AccountId
  group by [CityId],[City].Address 
--Task 5.3.Найти клиентов которые имеют счета в более чем половине филиалов
 
  SELECT
  [ClientId],
  Client.Name
  FROM [BankDB].[dbo].[Account]
  inner join  [BankDB].[dbo].[Fialation] as Fiolation on Fiolation.Id = FiolationId
  inner join [BankDB].[dbo].[Client] as Client on Client.Id = [ClientId]
  group by ClientId, Client.Name
  having Count(DISTINCT FiolationId) >= (SELECT (COUNT(*) + 1)/2 FROM [BankDB].[dbo].[Fialation])

--Task 5.4. Найти клиентов которых не обслуживали сатрудникки с заданым щаблоном фамилии
  
  SELECT [CityId]
      ,[AccountId]
      ,[Id]
      ,[Name]
  FROM [BankDB].[dbo].[Client]
  where Id not in (
  SELECT [ClientId]  FROM [BankDB].[dbo].[AccountOperation]
  inner join  [BankDB].[dbo].[Client] as client on client.Id = ClientId 
  where EmployeeId  in
  (SELECT TOP (1000) [Id]
    FROM [BankDB].[dbo].[Employee] where SecondName like '%o%'))


-- 6. лаба

--Task 6.1.Со счетов всех клиентов относящихся к конкретному филиалу(или региону)
--надо снять деньги за обслуживание в зависимотсти от типа счетов 

	update [BankDB].[dbo].[Account]
	set balance = balance - AccountTypeId * 10
	where FiolationId = 1

--Task 6.2.Услуги по которым обслуживание не проводилось в течение периода 
--(создать таблицу архивные услуги и все операции по ним удалить из бд)

  Delete from [BankDB].[dbo].[ArchiveServices]
  where [Date]  BETWEEN '1999-01-01' AND '2002-07-31'

--Task 6.3. Занести в отдельную таблицу клиентов, которые имеют хотя бы один счет
--с отрицательным балансом.

  insert into [BankDB].[dbo].[Debtors]
  (ClientId, InsertedDate)
  SELECT Client.Id, SYSDATETIME()
  FROM [BankDB].[dbo].[Client] as Client
  inner join [BankDB].[dbo].[Account] as Account on Account.Id = AccountId
  where Account.balance < 0
