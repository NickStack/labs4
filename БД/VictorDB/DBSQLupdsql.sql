USE [Сanteen]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Client](
	[Id] [int] NOT NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DeliverDeliverisReferences](
	[DelivierID] [int] NOT NULL,
	[DeleviriesID] [int] NOT NULL,
 CONSTRAINT [PK_DeliverDeliverisReferences] PRIMARY KEY CLUSTERED 
(
	[DelivierID] ASC,
	[DeleviriesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Deliveries](
	[Date] [date] NOT NULL,
	[Count] [int] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
 CONSTRAINT [PK_Deliveries] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Delivers](
	[FullName] [nvarchar](50) NOT NULL,
	[TelephoneNumber] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](50) NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Delivers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Dish](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Cost] [money] NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Size] [nvarchar](50) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Dish] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DishProductReference](
	[DishId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
 CONSTRAINT [PK_DishProductReference] PRIMARY KEY CLUSTERED 
(
	[DishId] ASC,
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Employee](
	[FullName] [nvarchar](50) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PassportNumber] [nvarchar](50) NOT NULL,
	[PositionId] [int] NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeOrderReference](
	[EmployeeId] [int] NOT NULL,
	[OrderId] [int] NOT NULL,
 CONSTRAINT [PK_EmployeeOrderReference] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC,
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Feedback](
	[Comment] [nvarchar](50) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
 CONSTRAINT [PK_Feedback] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ListOfDishes](
	[DishId] [int] NOT NULL,
	[Count] [int] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
 CONSTRAINT [PK_ListOfDishes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NOT NULL,
	[Cost] [int] NOT NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Position](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Salary] [money] NOT NULL,
 CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Product](
	[Name] [nvarchar](50) NOT NULL,
	[Count] [int] NOT NULL,
	[Dimension] [nvarchar](10) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Client] ([Id])
VALUES 
(1)
,(2)
,(3)
,(4)
,(5)
,(6)
,(7)
,(8)
,(9)
,(10)
,(11)
,(12)
,(13)
,(14)
,(15)
,(16)

INSERT [dbo].[DeliverDeliverisReferences] ([DelivierID], [DeleviriesID]) 
VALUES (1, 1)
,(1, 13)
,(2, 3)
,(2, 6)
,(2, 7)
, (2, 12)
,(2, 15)
,(3, 2)
,(3, 8)
,(3, 11)
,(3, 14)
,(4, 5)
,(4, 9)
,(4, 10)
SET IDENTITY_INSERT [dbo].[Deliveries] ON 

INSERT [dbo].[Deliveries] ([Date], [Count], [Id], [ProductId]) 
VALUES 
 (CAST(N'2009-12-12' AS Date), 5000, 1, 1)
,(CAST(N'2010-05-12' AS Date), 300, 2, 2)
,(CAST(N'2010-06-11' AS Date), 1000, 3, 3)
,(CAST(N'2019-01-07' AS Date), 2000, 4, 4)
,(CAST(N'2018-04-04' AS Date), 1200, 5, 5)
,(CAST(N'2017-01-12' AS Date), 1340, 6, 6)
,(CAST(N'2016-07-16' AS Date), 1270, 7, 7)
,(CAST(N'2015-01-02' AS Date), 1150, 8, 8)
,(CAST(N'2015-05-12' AS Date), 1340, 9, 9)
,(CAST(N'2012-11-11' AS Date), 1070, 10, 10)
,(CAST(N'2013-01-01' AS Date), 1035, 11, 11)
,(CAST(N'2010-07-12' AS Date), 1037, 12, 12)
,(CAST(N'2017-11-12' AS Date), 1153, 13, 13)
,(CAST(N'2018-05-12' AS Date), 1450, 14, 14)
,(CAST(N'2019-05-12' AS Date), 1300, 15, 15)
,(CAST(N'2011-08-11' AS Date), 1404, 16, 16)

SET IDENTITY_INSERT [dbo].[Deliveries] OFF
SET IDENTITY_INSERT [dbo].[Delivers] ON 

INSERT [dbo].[Delivers] ([FullName], [TelephoneNumber], [Status], [Address], [Id])
VALUES 
 (N'Eremin IP', N'80333594567', N'Active', NULL, 1)
,(N'Agaphonov OOO', N'+375295463768', N'Activee', N'Minsk pr-t Nezavisimosti 19', 2)
,(N'Sinezyss', NULL, N'Active', NULL, 3)
,(N'Potatoes World', N'80171838650', N'Active', N'ag. Naberezhniy', 4)

SET IDENTITY_INSERT [dbo].[Delivers] OFF
SET IDENTITY_INSERT [dbo].[Dish] ON 

INSERT [dbo].[Dish] ([Code], [Name], [Cost], [Type], [Size], [Id]) 
VALUES 
 (N'1556434', N'Roasted potatoes', 2.0000, N'Hot', N'M', 1)
,(N'1556435', N'Boild potatoes', 1.0000, N'Hot', N'M', 3)
,(N'1556436', N'RoastedPotatoes', 3.0000, N'Hot', N'L', 5)
,(N'1553437', N'Roasted Potatoes', 1.0000, N'Hot', N'S', 7)
,(N'1553438', N'Pancakes', 4.0000, N'Hot', N'M', 8)
,(N'1553439', N'Pancakes', 6.0000, N'Hot', N'L', 9)
,(N'1553440', N'Pancakes', 2.0000, N'Hot', N'S', 10)
,(N'1553441', N'Fish', 3.0000, N'Hot', N'S', 11)
,(N'1553442', N'Fish', 6.0000, N'Hot', N'M', 12)
,(N'1553443', N'Fish', 9.0000, N'Hot', N'L', 13)
,(N'1553444', N'Juice', 1.0000, N'Cold', N'S', 14)
,(N'1553445', N'Juice', 2.0000, N'Cold', N'M', 15)
,(N'1553446', N'Juice', 3.0000, N'Cold', N'L', 16)
,(N'1553447', N'Cola', 1.0000, N'Cold', N'S', 17)
,(N'1553448', N'Sprite', 1.0000, N'Cold', N'S', 18)
,(N'1553449', N'Fanta', 1.0000, N'Cold', N'S', 19)
,(N'1553450', N'Cola', 2.0000, N'Cold', N'M', 20)
,(N'1553451', N'Sprite', 2.0000, N'Cold', N'M', 21)
,(N'1553452', N'Fanta ', 2.0000, N'Cold', N'M', 22)
,(N'1553453', N'Cola', 3.0000, N'Cold', N'L', 23)
,(N'1553454', N'Sprite', 3.0000, N'Cold', N'L', 24)
,(N'1553455', N'Fanta', 3.0000, N'Cold', N'L', 25)

SET IDENTITY_INSERT [dbo].[Dish] OFF
SET IDENTITY_INSERT [dbo].[Employee] ON 

INSERT [dbo].[Employee] ([FullName], [Id], [PassportNumber], [PositionId]) 
VALUES 
 (N'Victor Shpigun', 2, N'MS2645179', 1)
,(N'Victori Anoshko', 3, N'MP2736489', 2)
,(N'Eugeni Kriveckiy', 4, N'MN9034561', 3)
,(N'Anton Zvorotnuk', 5, N'MB5734892', 4)
,(N'Roman Drozd', 6, N'TR1234125', 5)

SET IDENTITY_INSERT [dbo].[Employee] OFF
SET IDENTITY_INSERT [dbo].[Feedback] ON 

INSERT [dbo].[Feedback] ([Comment], [Id], [ClientId], [EmployeeId]) 
VALUES 
 (N'Rude employee', 1, 1, 3)
,(N'Polite employee', 3, 2, 2)
,(N'Rude employee', 4, 3, 3)
SET IDENTITY_INSERT [dbo].[Feedback] OFF
SET IDENTITY_INSERT [dbo].[ListOfDishes] ON 

INSERT [dbo].[ListOfDishes] ([DishId], [Count], [Id], [OrderId]) 
VALUES 
 (1, 1, 4, 1)
,(3, 1, 7, 1)
,(5, 1, 8, 1)
,(7, 1, 9, 1)
,(1, 2, 10, 2)
,(3, 2, 11, 2)
,(1, 1, 12, 3)
,(5, 3, 13, 5)
,(5, 4, 15, 4)
,(3, 4, 16, 4)
,(1, 10, 18, 4)
,(7, 10, 19, 4)
,(8, 1, 20, 7)
,(1, 2, 21, 6)
,(9, 3, 23, 6)
,(11, 12, 24, 8)
,(13, 14, 25, 8)
,(15, 12, 26, 9)
,(14, 5, 27, 9)
,(12, 4, 28, 10)
,(13, 4, 29, 10)
,(15, 7, 30, 11)
,(14, 5, 31, 11)
,(3, 4, 32, 11)
SET IDENTITY_INSERT [dbo].[ListOfDishes] OFF
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([Id], [ClientId], [Cost]) 
VALUES
 (1, 1, 2000)
,(2, 2, 3000)
,(3, 3, 4000)
,(4, 4, 500)
,(5, 5, 1100)
,(6, 6, 1200)
,(7, 7, 1130)
,(8, 8, 1340)
,(9, 9, 1270)
,(10, 10, 970)
,(11, 11, 7650)
,(12, 12, 850)
,(13, 13, 450)
, (14, 14, 580)
,(15, 15, 630)
SET IDENTITY_INSERT [dbo].[Order] OFF
SET IDENTITY_INSERT [dbo].[Position] ON 

INSERT [dbo].[Position] ([Id], [Name], [Salary])
VALUES 
 (1, N'Director', 2500.0000)
,(2, N'Cleaner', 700.0000)
,(3, N'Teller', 1000.0000)
,(4, N'Security', 1200.0000)
,(5, N'Heaver', 650.0000)
SET IDENTITY_INSERT [dbo].[Position] OFF
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([Name], [Count], [Dimension], [Id]) 
VALUES 
(N'Potatoes', 400, N'g', 1)
,(N'Cheese', 40, N'g', 2)
,(N'Carrot', 200, N'g', 3)
,(N'Raspberries', 100, N'g', 4)
,(N'Strawberry', 100, N'g', 5)
,(N'Flour', 200, N'g', 6)
,(N'Cola', 250, N'ml', 7)
,(N'Cola', 500, N'ml', 8)
,(N'Cola', 1000, N'ml', 9)
,(N'Sprite', 250, N'ml', 10)
,(N'Sprite', 500, N'ml', 11)
,(N'Sprite', 1000, N'ml', 12)
,(N'Fanta', 250, N'ml', 13)
,(N'Fanta', 500, N'ml', 14)
,(N'Fanta', 1000, N'ml', 15)
,(N'Fish', 200, N'g', 16)
,(N'Fish', 400, N'g', 17)
,(N'Fish', 600, N'g', 18)
,(N'Apple juice', 250, N'ml', 19)
,(N'Apple juice', 500, N'ml', 20)
,(N'Apple juice', 1000, N'ml', 21)
,(N'Orange juice', 250, N'ml', 22)
,(N'Orange juice', 500, N'ml', 23)
,(N'Orange juice', 1000, N'ml', 24)
,(N'Apple jam', 100, N'g', 25)
,(N'Orange jam', 100, N'g', 26)
,(N'Banan jam', 100, N'g', 27)
,(N'Potatoes', 200, N'g', 30)

SET IDENTITY_INSERT [dbo].[Product] OFF
ALTER TABLE [dbo].[DeliverDeliverisReferences]  WITH CHECK ADD  CONSTRAINT [FK_DeliverDeliverisReferences_Deliveries] FOREIGN KEY([DeleviriesID])
REFERENCES [dbo].[Deliveries] ([Id])
GO
ALTER TABLE [dbo].[DeliverDeliverisReferences] CHECK CONSTRAINT [FK_DeliverDeliverisReferences_Deliveries]
GO
ALTER TABLE [dbo].[DeliverDeliverisReferences]  WITH CHECK ADD  CONSTRAINT [FK_DeliverDeliverisReferences_Delivers] FOREIGN KEY([DelivierID])
REFERENCES [dbo].[Delivers] ([Id])
GO
ALTER TABLE [dbo].[DeliverDeliverisReferences] CHECK CONSTRAINT [FK_DeliverDeliverisReferences_Delivers]
GO
ALTER TABLE [dbo].[Deliveries]  WITH CHECK ADD  CONSTRAINT [FK_Deliveries_Product] FOREIGN KEY([Id])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[Deliveries] CHECK CONSTRAINT [FK_Deliveries_Product]
GO
ALTER TABLE [dbo].[DishProductReference]  WITH CHECK ADD  CONSTRAINT [FK_DishProductReference_Dish] FOREIGN KEY([DishId])
REFERENCES [dbo].[Dish] ([Id])
GO
ALTER TABLE [dbo].[DishProductReference] CHECK CONSTRAINT [FK_DishProductReference_Dish]
GO
ALTER TABLE [dbo].[DishProductReference]  WITH CHECK ADD  CONSTRAINT [FK_DishProductReference_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[DishProductReference] CHECK CONSTRAINT [FK_DishProductReference_Product]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Position] FOREIGN KEY([PositionId])
REFERENCES [dbo].[Position] ([Id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Position]
GO
ALTER TABLE [dbo].[EmployeeOrderReference]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeOrderReference_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[EmployeeOrderReference] CHECK CONSTRAINT [FK_EmployeeOrderReference_Employee]
GO
ALTER TABLE [dbo].[EmployeeOrderReference]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeOrderReference_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[EmployeeOrderReference] CHECK CONSTRAINT [FK_EmployeeOrderReference_Order]
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK_Feedback_Client] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Client] ([Id])
GO
ALTER TABLE [dbo].[Feedback] CHECK CONSTRAINT [FK_Feedback_Client]
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK_Feedback_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[Feedback] CHECK CONSTRAINT [FK_Feedback_Employee]
GO
ALTER TABLE [dbo].[ListOfDishes]  WITH CHECK ADD  CONSTRAINT [FK_ListOfDishes_Dish] FOREIGN KEY([DishId])
REFERENCES [dbo].[Dish] ([Id])
GO
ALTER TABLE [dbo].[ListOfDishes] CHECK CONSTRAINT [FK_ListOfDishes_Dish]
GO
ALTER TABLE [dbo].[ListOfDishes] 
WITH CHECK ADD  CONSTRAINT [FK_ListOfDishes_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[ListOfDishes] 
CHECK CONSTRAINT [FK_ListOfDishes_Order]
GO
ALTER TABLE [dbo].[Order] 
WITH CHECK ADD  CONSTRAINT [FK_Order_Client] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Client] ([Id])
GO
ALTER TABLE [dbo].[Order]
CHECK CONSTRAINT [FK_Order_Client]
GO