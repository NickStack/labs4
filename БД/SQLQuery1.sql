USE [master]
GO
/****** Object:  Database [BankDB]    Script Date: 21.11.2019 21:48:55 ******/
CREATE DATABASE [BankDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BankDB', FILENAME = N'D:\App\SQL\MSSQL14.SQLEXPRESS\MSSQL\DATA\BankDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BankDB_log', FILENAME = N'D:\App\SQL\MSSQL14.SQLEXPRESS\MSSQL\DATA\BankDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
USE [BankDB]
GO

CREATE TABLE [dbo].[Account](
	[balance] [money] NOT NULL,
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[ClientId] [int] NULL,
	[AccountTypeId] [int] NOT NULL,
	[FiolationId] [int] NOT NULL)
GO

CREATE TABLE [dbo].[AccountOperation](
	[Date] [date] NOT NULL,
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[TypeId] [int] NOT NULL,
	[Comment] [nvarchar](50) NOT NULL,
	[ClientId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[AccountId] [int] NOT NULL)
GO

CREATE TABLE [dbo].[AccountOperationReference](
	[AccountId] [int] NOT NULL,
	[OperationId] [int] NOT NULL,
 CONSTRAINT [PK_AccountOperationReference] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC,
	[OperationId] ASC
)ON [PRIMARY])
GO

CREATE TABLE [dbo].[AccountOperationType](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Name] [nvarchar](50) NOT NULL)
GO

CREATE TABLE [dbo].[AccountType](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Name] [nvarchar](50) NOT NULL)
GO

CREATE TABLE [dbo].[City](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Address] [nvarchar](100) NOT NULL)
GO

CREATE TABLE [dbo].[Client](
	[CityId] [int] NOT NULL,
	[AccountId] [int] NOT NULL,
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Name] [nvarchar](50) NOT NULL)
GO

CREATE TABLE [dbo].[ClientOperationReference](
	[OperationId] [int] NOT NULL,
	[ClientId] [int] NOT NULL,
  CONSTRAINT client_operation_pk PRIMARY KEY ([OperationId], [ClientId]),
  CONSTRAINT FK_Client 
      FOREIGN KEY ([ClientId]) REFERENCES [dbo].[Client] (Id),
  CONSTRAINT FK_category 
      FOREIGN KEY ([OperationId]) REFERENCES [dbo].[AccountOperation] (Id))
GO



CREATE TABLE [dbo].[Employee](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[PositionId] [int] NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[SecondName] [nvarchar](50) NOT NULL,
	[PassportNumber] [nvarchar](50) NOT NULL,
	[DateOfBirthday] [date] NOT NULL)
GO

CREATE TABLE [dbo].[Fialation](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[RegionName] [nvarchar](100) NOT NULL,
	[Address] [nvarchar](200) NOT NULL)
GO

CREATE TABLE [dbo].[Position](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Name] [nvarchar](50) NOT NULL)
GO

CREATE TABLE [dbo].[Service](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Comment] [nvarchar](50) NOT NULL,
	[Date] [date] NOT NULL,
	[TypeServiceId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[ClientId] [int] NOT NULL)
GO

CREATE TABLE [dbo].[ServiceType](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Name] [nvarchar](50) NOT NULL)
GO

ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_AccountType] FOREIGN KEY([AccountTypeId])
REFERENCES [dbo].[AccountType] ([Id])
GO

ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Client] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Client] ([Id])
GO

ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Fialation] FOREIGN KEY([FiolationId])
REFERENCES [dbo].[Fialation] ([Id])
GO

ALTER TABLE [dbo].[AccountOperation]  WITH CHECK ADD  CONSTRAINT [FK_AccountOperation_Account] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
GO

ALTER TABLE [dbo].[AccountOperation]  WITH CHECK ADD  CONSTRAINT [FK_AccountOperation_AccountOperationType] FOREIGN KEY([TypeId])
REFERENCES [dbo].[AccountOperationType] ([id])
GO

ALTER TABLE [dbo].[AccountOperation]  WITH CHECK ADD  CONSTRAINT [FK_AccountOperation_Client] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Client] ([Id])
GO

ALTER TABLE [dbo].[AccountOperation]  WITH CHECK ADD  CONSTRAINT [FK_AccountOperation_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO

ALTER TABLE [dbo].[AccountOperationReference]  WITH CHECK ADD  CONSTRAINT [FK_AccountOperationReference_Account] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
GO

ALTER TABLE [dbo].[AccountOperationReference]  WITH CHECK ADD  CONSTRAINT [FK_AccountOperationReference_AccountOperation] FOREIGN KEY([OperationId])
REFERENCES [dbo].[AccountOperation] ([Id])
GO

ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_Client_City] FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([Id])
GO

ALTER TABLE [dbo].[ClientOperationReference]  WITH CHECK ADD  CONSTRAINT [FK_ClientOperationReference_AccountOperation] FOREIGN KEY([OperationId])
REFERENCES [dbo].[AccountOperation] ([Id])
GO

ALTER TABLE [dbo].[ClientOperationReference]  WITH CHECK ADD  CONSTRAINT [FK_ClientOperationReference_Client] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Client] ([Id])
GO

ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Position] FOREIGN KEY([PositionId])
REFERENCES [dbo].[Position] ([Id])
GO

ALTER TABLE [dbo].[Service]  WITH CHECK ADD  CONSTRAINT [FK_Service_Client] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Client] ([Id])
GO

ALTER TABLE [dbo].[Service]  WITH CHECK ADD  CONSTRAINT [FK_Service_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO

ALTER TABLE [dbo].[Service]  WITH CHECK ADD  CONSTRAINT [FK_Service_ServiceType] FOREIGN KEY([TypeServiceId])
REFERENCES [dbo].[ServiceType] ([Id])

USE [master]
GO

ALTER DATABASE [BankDB] SET  READ_WRITE 
GO

USE
[BankDB]
GO

INSERT INTO [dbo].[ServiceType] 
(Name)
VALUES 
('Credit'),
('Refill'),
('Write-off'),
('Balans'),
('Transfer'),
('Convertation')
Go

INSERT INTO [dbo].[Position] 
(Name)
VALUES 
('Employee'),
('Manager'),
('Director'),
('Support-Employee'),
('It-Employee')
Go

INSERT INTO [dbo].[Employee] 
(PositionId, FirstName, SecondName, PassportNumber, DateOfBirthday)
VALUES 
(3,'Nikita','Stets','MS2737186', '19990528 10:34:09 AM'),
(2,'Roma','Lomaki','MP2888886', '19950620'),
(2,'Anton','Depp','MS2755586', '19940411'),
(4,'Aleksy','Habenskiy','ME4444186', '19930610'),
(4,'Sasha','Antonov','MB2731111', '19920703'),
(4,'Nikita','Grachenko','MA4818186', '19910612'),
(1,'Eugeniy','Glushko','MT2421184', '19901116'),
(1,'Andrey','Misichenko','MS0066999', '19891017'),
(1,'Artem','Drozd','MN3342313', '19870918'),
(5,'Zachar','Suharevich','MA2434146', '19860819'),
(5,'Natalia','Kuksa','MB2722131', '19850723'),
(5,'Karina','Selivonchik','MY2777788', '19840806')
Go

INSERT INTO [dbo].[Fialation] 
(RegionName,Address)
VALUES 
('Uzda', 'Sovetskayu 18a'),
('Sluck' , 'Lenina 14'),
('Soligorsk', 'Centralnyua 9'),
('Volkovisk', 'Naberezhnayu 114'),
('Bereza', 'Shkolnia 19')
Go

INSERT INTO [dbo].[City] 
(Address)
VALUES 
('Minsk'),
('Minsk obl'),
('Brest'),
('Brestskay obl'),
('Grodno'),
('Grodnenskay obl'),
('Vitebsk'),
('Vitebskay obl'),
('Gomel'),
('Gomel obl'),
('Mogilev'),
('Mogilevskay obl')
Go

INSERT INTO [dbo].[AccountType] 
(Name)
VALUES 
('Open'),
('Closed'),
('Freezzed')
go

INSERT INTO [dbo].[Account] 
(balance,AccountTypeId,FiolationId)
VALUES 
('1000', '1','1'),
('2000', '2','1'),
('3000', '3','2'),
('4000', '1','2'),
('5000', '2','1'),
('6000', '3','5'),
('7000', '1','4'),
('8000', '2','4'),
('9000', '3','2'),
('12000', '1','3'),
('12000', '2','5'),
('23000', '3','4'),
('33000', '1','3'),
('34000', '2','2'),
('123000', '3','1')
go

INSERT INTO [dbo].[Client] 
(Name, AccountId,CityId)
VALUES 
('Nikita', '1','1'),
('Artem', '2','1'),
('Roman', '3','2'),
('Eugeniy', '4','2'),
('Andrey', '5','1'),
('Alexey', '6','5'),
('Sergey', '7','4'),
('Karina', '8','4'),
('Victoria', '9','2'),
('Daria', '10','3'),
('Elina', '11','5'),
('Sasha', '12','4'),
('Anton', '13','3'),
('Artem', '14','2'),
('Mohamed', '15','1')
go

INSERT INTO [dbo].[Service] 
(Comment,Date,TypeServiceId,EmployeeId,ClientId)
VALUES 
('Check Balans','19990528 10:34:09 AM', 4, 2, 3),
('USD to EUR','19990528 10:34:09 AM', 6, 2, 2),
('USD to RUB','19990528 10:34:09 AM', 6, 3, 5),
('Approve Credit','19990528 10:34:09 AM', 1, 8, 9),
('Declined Credit','19990528 10:34:09 AM', 1, 5, 3)
go


INSERT INTO [dbo].[AccountOperationType] 
(Name)
VALUES 
('Open'),
('Close'),
('Freezz')
go

INSERT INTO [dbo].[AccountOperation] 
(Date,TypeId,Comment,ClientId,EmployeeId,AccountId)
VALUES 
('19990528 10:12:09 AM', 2, 'Bankrot', 4, 2, 4),
('19990528 11:34:12 AM', 1, 'New client', 2, 2,2),
('19990528 08:15:11 AM', 3,'Problemi s zakonom', 5, 3, 5),
('19990528 09:05:01 AM', 2,'Go away to another bank',9, 8, 9),
('19990528 07:09:04 AM', 1,'-',3, 5, 6)
go